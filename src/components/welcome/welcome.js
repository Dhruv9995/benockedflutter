import React from 'react';
import LeftChamber from './../leftChamber/leftChamber';
import { Container, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import './welcome.css'


const Welcome = () => {
    let name = JSON.parse(localStorage.getItem('userData'))['username'];
    return (
        <div>
            <Container fluid={true} className='login'>
                <ToastContainer />
                <Row>
                    <div className="col-md-6 EmailVerified">
                        <LeftChamber color='rgb(249, 217, 235)' imageURL='verified.png' fontColor='black' />
                    </div>
                    <div className="col-md-6 " >
                        <div className="container verifiedrightText">
                            <div className=" verifiedRight text-left"  >
                                <h1>Hey {name} !</h1><br />
                                <h4>  Welcome to <span style={{ fontSize: '40px', color: 'black', fontWeight: 'bold' }}> benocked !</span>
                                    <br />
                                You have logged in successfully for the first time.
                                <br />
                                We welcome you to benocked family. Enjoy and get ready for benocked
                                </h4>
                                <p style={{ fontSize: '12px' }} >You still need to complete your profile first to enjoy all the features of our website, So please complete it as soon as possible.</p>
                                <Link to="/social" style={{ float: 'right' }} className="getstarted ">Next</Link>
                            </div>
                        </div>
                    </div>
                </Row>
            </Container>
        </div >
    );
}

export default Welcome;