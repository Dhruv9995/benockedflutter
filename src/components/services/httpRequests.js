import axios from 'axios';

const path = 'http://benocked.in/accounts'
// const path = 'http://6b9a8a6755d3.ngrok.io/accounts'

class HTTPRequests {
    registerUser(data) {
        return axios.post(path + '/register', data);
    }
    loginUser(data) {
        return axios.post(path + '/login', data);
    }
    userData(data) {
        data = this.getHeaders(data)
        return axios.get(path + '/own', data);
    }
    otherUserData(id) {
        let data = this.getHeaders({})
        return axios.get(path + '/user/' + id, data);
    }
    allUsers(data) {
        data = this.getHeaders(data)
        return axios.get(path + '/users', data);
    }
    followUsers(id) {
        let data = this.getHeaders({})
        return axios.get(path + '/send/' + id, data);
    }
    unfollowUsers(id) {
        let data = this.getHeaders({})
        return axios.get(path + '/unfollow/' + id, data);
    }
    completeProfile(data) {
        return axios.post(path + '/completeProfile', data, {
            headers: {
                Authorization: 'JWT ' + localStorage.getItem('token')
            }
        });
    }
    postPosts(data) {
        return axios.post(path + '/post/update', data, {
            headers: {
                Authorization: 'JWT ' + localStorage.getItem('token')
            }
        });
    }
    getPosts(data) {
        data = this.getHeaders(data)
        return axios.get(path + '/post/posts', data);
    }
    likePost(id) {
        let data = this.getHeaders({})
        return axios.get(path + `/post/like/${id}`, data);
    }
    getHeaders(data) {
        data['headers'] = {
            Authorization: 'JWT ' + localStorage.getItem('token')
        }
        return data;
    }
}

export default HTTPRequests;