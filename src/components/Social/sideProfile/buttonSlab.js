import React, { Component } from 'react';
import './../social.css'
class ButtonSlab extends Component {
    state = {}
    render() {
        return (
            <div>
                <div className="row no-gutters mt-2 mb-2 align-items-center justify-content-center">
                    <div className="card" style={{ border: '0px' }}>
                        <div className="card-body">
                            <div className="btn-group btn-group-justified" role="group" aria-label="Button group">

                                <div className="btn-group">
                                    <button className="btn btn-outline-primary ml-1 mr-1 mt-1 mb-1">My Subs</button>
                                </div>
                                <div className="btn-group">
                                    <button className="btn btn-outline-primary ml-1 mr-1 mt-1 mb-1">My Page</button>
                                </div>
                                <div className="btn-group">
                                    <button className="btn btn-outline-primary ml-1 mr-1 mt-1 mb-1">My followers</button>
                                </div>
                                <div className="btn-group">
                                    <button className="btn btn-outline-primary ml-1 mr-1 mt-1 mb-1">My wallet</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default ButtonSlab;