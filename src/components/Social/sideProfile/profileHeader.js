import React, { Component } from 'react';
import { Collapse } from 'react-collapse';
import './../social.css'

class ProfileHeader extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }
    }
    toggle() {
        this.setState({ isOpen: !this.state.isOpen })
    }
    shouldComponentUpdate() {
        if (localStorage.getItem('cardRefresh') || this.state.isOpen) {
            localStorage.setItem("cardRefresh", false);
            return true;
        }
        return false;
    }
    render() {
        let userData = JSON.parse(localStorage.getItem('userData'))
        return (
            <div>
                <div className="row no-gutters align-items-center justify-content-center">
                    <div className="col-md-3">
                        <a href='/profile' style={{ color: 'black', textDecoration: 'none' }}> <img className="my-Avatar Avatar img-fluid Avatarcontainer" src={require('./../../assets/images/boyprofile.jpg')} alt='profile'></img></a>
                    </div>
                    <div className="col-md-8 text-left">
                        <div className="container feedbox-head text-left" >
                            <h5 ><a href='/profile' style={{ color: 'black', textDecoration: 'none' }}>{userData.fullname || userData.username}</a></h5>
                            <small >{userData.city}</small>
                        </div>
                    </div>
                    <div className="col-md-1">
                        <div className="profileshow collapsed" onClick={this.toggle.bind(this)} />
                    </div>
                    <Collapse isOpened={this.state.isOpen}>
                        <div className="col-md">
                            <div id="accordion" className="accordion" >
                                <div id="collapseOne" className=" card-body" data-parent="#accordion">
                                    <div className="card" style={{ border: '0px' }}>
                                        <div className="card-body">
                                            <div className="h5">@{userData.username}</div>
                                            <div className="h7 text-muted"><label className='h6 text-muted'>Fullname:</label> {userData.fullname}</div>
                                            <div className="h7">
                                                {userData.qualifications && userData.qualifications.join(', ')}
                                            </div>
                                        </div>
                                        <ul className="list-group list-group-flush">
                                            <li className="list-group-item">
                                                <div className="h6 text-muted">Followers</div>
                                                <div className="h5">{userData.followers_count}</div>
                                            </li>
                                            <li className="list-group-item">
                                                <div className="h6 text-muted">Following</div>
                                                <div className="h5">{userData.following_count}</div>
                                            </li>
                                            <li className="list-group-item "><label className='h6 text-muted'>Current Institution:</label> {userData.college && userData.current_course ? userData.college + ', ' + userData.current_course : 'Please complete your profile'}
                                                <h1 >About</h1>
                                                <p className="lead">{userData.about ? userData.about : 'Please fill about section in your profile'}</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Collapse>
                </div>
            </div >
        );
    }
}

export default ProfileHeader;