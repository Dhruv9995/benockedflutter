import React, { Component } from "react";
import DownDropDown from "../../../../downArrowDropDown/downDropDown";
import HTTPRequests from "./../../../../services/httpRequests";

class PeopleCards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      follow: this.props.follow
    };
    this.service = new HTTPRequests();
  }
  followUser() {
    if (this.state.follow)
      this.service.followUsers(this.props.userId).then((response) => {
        if (response.status === 200) {
          this.setState({ follow: false });
          this.service.userData({}).then((response) => {
            if (response.status === 200) {
              localStorage.removeItem("userData");
              localStorage.setItem("userData", JSON.stringify(response.data));
              localStorage.setItem("cardRefresh", true);
              if (this.props.handle()) this.props.handle();
            }
          });
        }
      });
    else
      this.service.unfollowUsers(this.props.userId).then((response) => {
        if (response.status === 200) {
          this.setState({ follow: true });
          this.service.userData({}).then((response) => {
            if (response.status === 200) {
              localStorage.removeItem("userData");
              localStorage.setItem("userData", JSON.stringify(response.data));
              localStorage.setItem("cardRefresh", true);
              if (this.props.handle()) this.props.handle();
            }
          });
        }
      });
  }
  render() {
    return (
      <div>
        <div className="form-group tab-content">
          <div className="card mt-2">
            <div className="card-body">
              <div className="trend-card">
                {/* <DownDropDown
                  title="Action"
                  buttons={[
                    { name: "Hide", link: "#" },
                    { name: "Report", link: "#" },
                  ]}
                /> */}
              </div>
              <div className="row mt-4 md-1 no-gutters align-items-center justify-content-center">
                <div className="col-md-3 ">
                  <img
                    className=" Avatar img-fluid Avatarcontainer"
                    src={this.props.profilePic} alt="profile pic"
                  ></img>
                </div>
                <div className="col-md-6">
                  <div className="  container feedbox-head text-left">
                    <a href={"/user/" + this.props.userId}>
                      <h5>{this.props.followUser}</h5>
                    </a>
                    {this.props.isPost && (
                      <small>
                        {this.props.isPost.text}
                        <br />
                        <a href={this.props.isPost.link}>jump to post</a>
                        <br />
                        <br />
                      </small>
                    )}
                    <small>{this.props.location}</small>
                  </div>
                </div>
                <div className="col-md-3">
                  {Number(JSON.parse(localStorage.getItem("userData"))["id"]) !==
                    Number(this.props.userId) && (
                      <div
                        className="btn mt-auto mb-auto ml-auto mr-auto btn-outline-primary"
                        type="button"
                        id="follow"
                        value={this.props.userId}
                        onClick={this.followUser.bind(this)}
                      >
                        {this.state.follow ? "Follow" : "Unfollow"}
                      </div>
                    )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PeopleCards;
