import React, { Component } from 'react';
import { Tabs, Tab, ResponsiveEmbed } from 'react-bootstrap';
import PeopleCards from './trendingCards/peopleCard';
import HTTPRequests from './../../../services/httpRequests';

class Trending extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: '',
            firstHit: true
        }
        this.service = new HTTPRequests()
    }

    fetchUsers() {
        if (this.state.firstHit) {
            this.service.allUsers({})
                .then((response) => {
                    if (response.status) {
                        this.setState({ users: response.data.Users_to_Follow, firstHit: false });
                    }
                })
        }
    }

    render() {
        this.fetchUsers()
        return (
            <div>
                <div className="row no-gutters mt-4 align-items-center justify-content-center">
                    <div className="col-md">
                        <div className='card' >
                            <div className="card-header">
                                <p className="h6 lead text-muted">Trending</p>
                            </div>
                            <div className="row">
                                <div className="col-md">
                                    <div className="card-body">
                                        <Tabs className="card-header tab" defaultActiveKey="People" id="uncontrolled-tab-example">
                                            <Tab eventKey="People" title="People">
                                                {
                                                    this.state.users && this.state.users.length > 0 &&
                                                    this.state.users.map((user) => {
                                                        return <PeopleCards follow={true} followUser={user.Name} userId={user.id} profilePic={user.profilePic} location='Chandigarh, India' key={user.id} />
                                                    })
                                                }
                                            </Tab>
                                            {/* <Tab eventKey="Posts" title="Posts">
                                                <PeopleCards followUser='Ankit' location='Chandigarh, India' isPost={{ text: 'this is sample text, it can be anything', link: '#' }} />
                                            </Tab>
                                            <Tab eventKey="Pages" title="Pages">
                                                <PeopleCards followUser='Ankit' location='Chandigarh, India' isPost={{ text: 'this is sample text, it can be anything', link: '#' }} />
                                            </Tab> */}
                                        </Tabs>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default Trending;