import React, { Component } from "react";
import { Tabs, Tab } from "react-bootstrap";
import HTTPRequests from "./../../services/httpRequests";

class AddFeed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: { text: "", image: "", video: "" },
    };
    this.service = new HTTPRequests();
  }

  handleChange(field, e) {
    let fields = this.state.fields;
    if (field === "image") fields[field] = e.target.files[0];
    else fields[field] = e.target.value;
    this.setState({ fields });
  }

  submitForm(form) {
    form.preventDefault();
    let formData = new FormData();
    let fields = this.state.fields;
    if (fields.text !== "" || fields.image !== "") {
      if (fields.text !== "") formData.append("title", fields.text);
      if (fields.image !== "") formData.append("image", fields.image);
      this.service.postPosts(formData).then((response) => {
        if (response.status === 200) {
          this.props.handler()
        }
      });
    }
    this.setState({ fields: { image: "", text: "" } });
  }

  render() {
    return (
      <div className="container mt-10">
        <form className="form-group" onSubmit={this.submitForm.bind(this)}>
          <div className="card gedf-card container-fluid rounded-lg">
            <Tabs
              className=" tab"
              defaultActiveKey="Make a publication"
              id="uncontrolled-tab-example"
            >
              <Tab eventKey="Make a publication" title="Make a publication">
                <div className="form-group">
                  <label className="sr-only" htmlFor="message">
                    post
                  </label>
                  <textarea
                    className="form-control"
                    id="message"
                    rows="3"
                    placeholder="What are you thinking?"
                    name="text"
                    value={this.state.fields["text"]}
                    onChange={this.handleChange.bind(this, "text")}
                  />
                </div>
              </Tab>
              <Tab eventKey="Images / Video" title="Images / Video">
                <div className="form-group">
                  <div className="custom-file">
                    <input
                      type="file"
                      className="custom-file-input"
                      id="customFile"
                      name="image"
                      //   value={this.state.fields["image"]}
                      onChange={this.handleChange.bind(this, "image")}
                    />
                    <label className="custom-file-label" htmlFor="customFile">
                      Upload image/Video
                    </label>
                  </div>
                </div>
              </Tab>
            </Tabs>
            <div className="card-body">
              <div className="btn-toolbar justify-content-between">
                <div className="btn-group">
                  <button
                    type="submit"
                    className="btn btn-primary"
                  // onClick={this.props.handler}
                  >
                    share
                  </button>
                </div>
                {/* <div className="btn-group">
                  <button
                    className="btn btn-link dropdown-toggle "
                    type="button"
                    id="gedf-drop1"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <img
                      style={{ float: "right" }}
                      src={require("./../../assets/svg/shield.svg")}
                      alt="shield"
                    />
                  </button>
                  <div
                    className="dropdown-menu dropdown-menu-right"
                    aria-labelledby="btnGroupDrop1"
                  >
                    <a className="dropdown-item" href="#">
                      <i className="fa fa-globe"></i> Public
                    </a>
                    <a className="dropdown-item" href="#">
                      <i className="fa fa-users"></i> Friends
                    </a>
                    <a className="dropdown-item" href="#">
                      <i className="fa fa-user"></i> Just me
                    </a>
                  </div>
                </div> */}
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default AddFeed;
