import React, { Component } from "react";
import MainNavBar from "./../mainNavBar/mainNavBar";
import AddFeed from "./editFeed/editFeed";
import SocialCard from "./socialCards/socialCards";
import SocialVideoCards from "./socialCards/socialVideoCards";
import SocialTextCards from "./socialCards/socialTextCards";
import ProfileHeader from "./sideProfile/profileHeader";
import Trending from "./sideProfile/trending/trending";
import HTTPRequests from "./../services/httpRequests";
import Loader from 'react-loader-spinner';
import { Redirect } from 'react-router-dom';
import logout from './../../functions';
import "./social.css";

class Social extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fetchedPosts: false,
      loading: true,
      posts: [],
      rerender: false,
      expired: false
    };
    this.service = new HTTPRequests();
    this.handler = this.handler.bind(this);
  }

  handler() {
    this.setState({ fetchedPosts: false });
    this.componentDidMount();
  }
  componentDidMount() {
    this.service.getPosts({}).then((response) => {
      if (response.status === 200) {
        let posts = response.data.posts;
        posts.sort(
          (a, b) =>
            new Date(b["created_on"]).getTime() -
            new Date(a["created_on"]).getTime()
        );
        this.setState({ posts: posts, fetchedPosts: true, loading: false });
      }
    }, async (postError) => {
      if (postError.response) {
        let response = postError.response
        if (response.status == 401) {
          await logout();
          this.setState({ expired: true });
        }
      }
    });
  }

  render() {
    if (this.state.expired) {
      return <Redirect to='/login' />;
    }
    return (
      <div>
        {this.state.loading ? (
          <div style={{ top: '40%', bottom: '40%', left: '40%', right: '40%', position: 'absolute' }}>
            <Loader type="Grid" color="#ea4c89" height={200} width={200} />
          </div>
        ) : (
            <div>
              <div className="container-fluid">
                <MainNavBar />
              </div>
              <div className="container-fluid socialLayer">
                <div className="row no-gutters mt-6">
                  <div className="col-md-5 offset-1 ">
                    <AddFeed handler={this.handler} />
                    {
                      this.state.posts &&
                        this.state.posts.length > 0 ?
                        this.state.posts.map((post) => {
                          if (post.title !== "" && post.image === "") {
                            return (
                              <SocialTextCards
                                avatar="profile.jpg"
                                username={post.user_details.name}
                                location={post.user_details.city}
                                time={(
                                  (new Date() - new Date(post.created_on)) /
                                  (60 * 1000)
                                ).toFixed()}
                                caption={post.title}
                                key={post.id}
                                id={post.id}
                                liked={post.liked_by_own}
                              />
                            );
                          }
                          if (post.image !== "") {
                            return (
                              <SocialCard
                                avatar="profile.jpg"
                                username={post.user_details.name}
                                location={post.user_details.city}
                                time={(
                                  (new Date() - new Date(post.created_on)) /
                                  (60 * 1000)
                                ).toFixed()}
                                postedPic={post.image}
                                caption={post.title}
                                key={post.id}
                                id={post.id}
                                liked={post.liked_by_own}
                              />
                            );
                          }
                          if (post.video !== "") {
                            // return (
                            // <SocialVideoCards
                            //   avatar="profile.jpg"
                            //   username="Ankita"
                            //   location="Chandigarh, India"
                            //   time="40"
                            //   caption="adshfsdfglisdvldifbvlidfgndfkjvdflhgsdljkvdlgfvdlsjkgblidfh"
                            //   videoLink="https://www.youtube.com/embed/v64KOxKVLVg"
                            // />
                            // );
                          }
                          return 0;
                        }) : null
                    }
                    {/* <SocialVideoCards avatar='profile.jpg' username='Ankita' location='Chandigarh, India' time='40' caption='adshfsdfglisdvldifbvlidfgndfkjvdflhgsdljkvdlgfvdlsjkgblidfh' videoLink='https://www.youtube.com/embed/v64KOxKVLVg' />
              <SocialTextCards avatar='profile.jpg' username='Ankita' location='Chandigarh, India' time='40' caption='adshfsdfglisdvldifbvlidfgndfkjvdflhgsdljkvdlgfvdlsjkgblidfh' /> */}
                  </div>
                  <div className="col-md-5 offset-1 mt-4 ">
                    <div className="container">
                      <div className="container-fluid feedbox rounded-lg">
                        <ProfileHeader />
                      </div>
                      {/* <div className="container-fluid feedbox rounded-lg" >
                  <ButtonSlab />
                </div> */}
                      <div className="container-fluid feedbox rounded-lg">
                        <Trending />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
      </div>
    );
  }
}

export default Social;