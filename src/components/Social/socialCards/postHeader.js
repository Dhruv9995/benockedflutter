import React, { Component } from 'react';
import DownDropDown from './../../downArrowDropDown/downDropDown';

class Header extends Component {
    state = {
        isOpen: false
    }
    toggleOpen = () => this.setState({ isOpen: !this.state.isOpen });
    render() {
        const menuClass = `dropdown-menu${this.state.isOpen ? " show" : ""}`;
        return (
            <div>
                <div className="d-flex justify-content-between align-items-center">
                    <div className="mr-2">
                        <img className=" Avatar img-fluid Avatarcontainer" src={require('./../../assets/images/' + this.props.avatar)} alt={this.props.avatar} />
                    </div>
                    <div className="ml-2">
                        <div className="h5 m-0">{this.props.username}</div>
                        <div className="h7 text-muted">{this.props.location}</div>
                    </div>
                    <div style={{ width: '200px' }}></div>
                    {/* <DownDropDown title='Configuration' buttons={[{ name: 'Save', link: '#' }, { name: 'Hide', link: '#' }, { name: 'Report', link: '#' }]} key={this.props.avatar} /> */}
                </div>
            </div>
        );
    }
}

export default Header;