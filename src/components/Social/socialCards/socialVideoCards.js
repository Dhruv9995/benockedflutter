import React, { Component } from 'react';
import LikeCommentShare from './likeCommentShare';
import Header from './postHeader';

class SocialVideoCards extends Component {
    render() {
        return (
            <div>
                <div className="row mt-4  ">
                    <div className="col-md-12">
                        <div className="card  ">
                            <div className="card-header">
                                <Header avatar={this.props.avatar} username={this.props.username} location={this.props.location} />
                            </div>
                            <div className="card-body ">
                                <div className="text-muted h7 mb-2">
                                {Number(this.props.time)>59?`..${((this.props.time)/60).toFixed()} hours ago`:`..${this.props.time} mins ago`}
                                </div>
                                <p className="card-title">
                                    {this.props.caption}
                                </p>
                                <div className="embed-responsive embed-responsive-16by9">
                                    {/* <iframe className="embed-responsive-item" src={this.props.videoLink} allowFullScreen></iframe> */}
                                </div>
                            </div>
                            <LikeCommentShare />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SocialVideoCards