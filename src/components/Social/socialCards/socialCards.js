import React, { Component } from 'react';
import LikeCommentShare from './likeCommentShare';
import Header from './postHeader';
import { calculateTime } from './socialHelpers';

class SocialCards extends Component {
    render() {
        return (
            <div>
                <div className="row mt-4">
                    <div className="col-md-12">
                        <div className="card socialCard">
                            <div className="card-header">
                                <Header avatar={this.props.avatar} username={this.props.username} location={this.props.location} />
                            </div>
                            <div className="card-body">
                                <div className="text-muted h7 mb-2">
                                    {calculateTime(this.props.time)}
                                </div>
                                <p className="card-title">
                                    {this.props.caption}
                                </p>
                                <img className="img-fluid" src={'http://benocked.in' + this.props.postedPic} alt="posted" />
                            </div>
                            <LikeCommentShare id={this.props.id} key={this.props.id} liked={this.props.liked} />
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default SocialCards;