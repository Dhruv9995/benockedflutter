function calculateTime(min) {
    let time = (min == 0 || min == 1) ? `...${min} min ago` : `...${min} mins ago`;
    let units = min
    if (min > 59) {
        units = (min / 60).toFixed();
        units == 1 || units == 0 ? time = `...${units} hour ago` : time = `...${units} hours ago`
        if (units > 23) {
            units = (units / 24).toFixed();
            units == 1 || units == 0 ? time = `...${units} day ago` : time = `...${units} days ago`;
            if (units > 365) {
                units = (units / 365).toFixed();
                units == 1 || units == 0 ? time = `...${units} year ago` : time = `...${units} years ago`;
            }
        }
    }
    return time;
}

exports.calculateTime = calculateTime;