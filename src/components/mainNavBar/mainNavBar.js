import React from 'react';
import './mainNavBar.css';

const MainNavBar = () => {
    return (
        <div>
            <nav className="navbar navbar-expand-md navbar-light bg-faded">
                <div className="mx-auto order-0">
                    <a className="navbar-brand mx-auto" href="/social">
                        <p style={{
                            padding: '12px 30px 0px 5px', fontSize: '40px', fontFamily: 'Sacramento ,cursive'
                        }}> benocked</p>
                    </a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                </div>


                <div className="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            {/* <a className="nav-link" href="/">Explore
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z" />
                                </svg>
                            </a> */}
                        </li>
                    </ul>
                </div>
                <div className="navbar-collapse collapse w-100 order-2 order-md-0 dual-collapse2">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <form>
                                <div className="input-group">
                                    {/* <input type="text" className="form-control" placeholder="Search" /> */}
                                    <div className="input-group-btn" >
                                        {/* <button className="btn btn-default form-control" type="submit">
                                            <svg id="Component_21_1" data-name="Component 21 – 1" xmlns="http://www.w3.org/2000/svg" width="13.03" height="13.03" viewBox="0 0 13.03 13.03">
                                                <g id="Ellipse_4" data-name="Ellipse 4" fill="#fff" stroke="rgba(126,125,125,0.57)" strokeWidth="1.5">
                                                    <circle cx="5.5" cy="5.5" r="5.5" stroke="none" />
                                                    <circle cx="5.5" cy="5.5" r="4.75" fill="none" />
                                                </g>
                                                <line id="Line_1" data-name="Line 1" x2="3" y2="3" transform="translate(9.5 9.5)" fill="none" stroke="rgba(126,125,125,0.57)" strokeWidth="1.5" />
                                            </svg>
                                        </button> */}
                                    </div>
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>

                <div className="navbar-collapse collapse w-100 order-3 dual-collapse2">
                    <ul className="navbar-nav ml-auto">
                        {/* <li className="nav-item">
                            <svg style={{ margin: "9px 9px 0px 0px" }} xmlns="http://www.w3.org/2000/svg" width="33.12" height="22.53" viewBox="0 0 33.12 22.53">
                                <g id="Component_11_2" data-name="Component 11 – 2" transform="translate(0.716 0.5)">
                                    <path id="Path_50" data-name="Path 50" d="M27.916,3.588H3.988l11.964,8.971ZM0,3.588A3.815,3.815,0,0,1,3.988,0H27.916A3.815,3.815,0,0,1,31.9,3.588V17.942a3.815,3.815,0,0,1-3.988,3.588H3.988A3.815,3.815,0,0,1,0,17.942Z" fill="#fff" stroke="#0e0006" strokeWidth="1" fillRule="evenodd" />
                                    <text id="bn" transform="matrix(0.998, -0.07, 0.07, 0.998, 1.188, 17.425)" fill="#040002" fontSize="14" fontFamily="Mistral"><tspan x="0" y="0">bn</tspan></text>
                                </g>
                            </svg>
                        </li>
                        <li className="nav-item"  >
                            <svg style={{ margin: '8px 5px 0px 5px' }} xmlns="http://www.w3.org/2000/svg" width="24.715" height="26.008" viewBox="0 0 24.715 26.008">
                                <g id="Component_12_1" data-name="Component 12 – 1" transform="translate(0.528 0.5)">
                                    <path id="Path_1" data-name="Path 1" d="M22.278,21.882H14.842a3.062,3.062,0,0,1-2.974,3.126,3.062,3.062,0,0,1-2.974-3.126H1.457A1.443,1.443,0,0,1,.118,20.788a1.724,1.724,0,0,1,.446-1.719,6.406,6.406,0,0,0,2.38-5V9.378A9.186,9.186,0,0,1,11.867,0a9.186,9.186,0,0,1,8.923,9.378v4.689a6.406,6.406,0,0,0,2.38,5,1.528,1.528,0,0,1,.446,1.719A1.443,1.443,0,0,1,22.278,21.882Z" transform="translate(-0.063)" fill="#fff" stroke="#010001" strokeWidth="1" fillRule="evenodd" />
                                </g>
                            </svg>
                        </li> */}

                        <li className="nav-item" style={{ marginTop: '2px' }}>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="41.67" height="43.01" viewBox="0 0 41.67 43.01">
                                <defs>
                                    <filter id="Union_1" x="0" y="0" width="41.67" height="43.01" filterUnits="userSpaceOnUse">
                                        <feOffset dy="3" input="SourceAlpha" />
                                        <feGaussianBlur stdDeviation="3" result="blur" />
                                        <feFlood floodColor="#fff" floodOpacity="0.42" />
                                        <feComposite operator="in" in2="blur" />
                                        <feComposite in="SourceGraphic" />
                                    </filter>
                                </defs>
                                <a href='/profile'>
                                    <g id="Profile" transform="translate(9.5 6.5)">
                                        <g transform="matrix(1, 0, 0, 1, -9.5, -6.5)" filter="url(#Union_1)">
                                            <path id="Union_1-2" data-name="Union 1" d="M0,24.01v-3c0-3.3,5.1-6,11.335-6s11.335,2.7,11.335,6v3ZM5.667,6a5.843,5.843,0,0,1,5.668-6A5.843,5.843,0,0,1,17,6a5.842,5.842,0,0,1-5.667,6A5.842,5.842,0,0,1,5.667,6Z" transform="translate(9.5 6.5)" fill="#fff" stroke="#010001" strokeWidth="1" />
                                        </g>
                                    </g>
                                </a>
                            </svg>
                        </li>

                        <li className="nav-item">
                            <a className="nav-link button" style={{ color: 'white', fontWeight: 'bold' }} href="/">ClassRoom</a>
                        </li>
                    </ul>
                </div >
            </nav >
        </div >
    );
}

export default MainNavBar;