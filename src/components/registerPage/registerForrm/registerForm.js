import React from 'react';
import HTTPRequests from './../../services/httpRequests';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Redirect } from 'react-router-dom';
import customToast from './../../toaster/toaster';

class Form extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fields: {
                userType: '',
                fullName: '',
                email: '',
                password: '',
                number: ''
            },
            errors: {},
            conditions: false,
            next: false
        }

        this.service = new HTTPRequests();
    }

    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        //usertype
        if (fields["userType"] === '') {
            formIsValid = false;
            errors["userType"] = "Please select something from the dropdown";
        }

        //Name
        if (!fields["fullName"]) {
            formIsValid = false;
            errors["fullName"] = "Name can not be empty";
        }

        if (typeof fields["fullName"] !== "undefined") {
            if (!fields["fullName"].match(/^[a-zA-Z0-9]+$/)) {
                formIsValid = false;
                errors["fullName"] = "Please enter a valid name";
            }
        }

        //Email
        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "Email can not be empty";
        }

        if (typeof fields["email"] !== "undefined") {
            let lastAtPos = fields["email"].lastIndexOf('@');
            let lastDotPos = fields["email"].lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["email"] = "Email is not valid";
            }
        }

        // password
        if (!fields["password"]) {
            formIsValid = false;
            errors['password'] = "Password is required"
        }

        if (fields['number'].length < 10) {
            formIsValid = false;
            errors['password'] = "Phone number should be atleast 10 digit long"
        }

        if (fields['number'].length !== 10) {
            formIsValid = false;
            errors['number'] = "Phone number should be 10 digit long"
        }
        this.setState({ errors: errors });
        return formIsValid;
    }

    stepValidation(field) {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        // userType
        if (field === "userType") {
            if (fields["userType"] === '') {
                formIsValid = false;
                errors["userType"] = "Please select something from the dropdown";
            }
        }

        //Name
        if (field === "fullName") {
            if (!fields["fullName"]) {
                formIsValid = false;
                errors["fullName"] = "Name can not be empty";
            }
            if (!fields["fullName"].match(/^[a-zA-Z0-9]+$/)) {
                formIsValid = false;
                errors["fullName"] = "Please enter a valid name";
            }
        }

        // email
        if (field === 'email') {
            if (!fields["email"]) {
                formIsValid = false;
                errors["email"] = "Email can not be empty";
            }

            if (typeof fields["email"] !== "undefined") {
                let lastAtPos = fields["email"].lastIndexOf('@');
                let lastDotPos = fields["email"].lastIndexOf('.');

                if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                    formIsValid = false;
                    errors["email"] = "Email is not valid";
                }
            }
        }

        if (field === 'password') {
            if (!fields["password"]) {
                formIsValid = false;
                errors['password'] = "Password is required"
            }
        }

        if (field === 'number') {
            if (fields['number'].length !== 10) {
                formIsValid = false;
                errors['number'] = "Phone number should be 10 digit long"
            }
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
        this.stepValidation(field);
    }

    enableButton() {
        if (this.state.conditions) {
            this.setState({ conditions: false });
            document.getElementById("registerSubmit").disabled = false;
        } else {
            this.setState({ conditions: true });
            document.getElementById("registerSubmit").disabled = true;
        }
    }

    submitForm(form) {
        form.preventDefault();
        let isValidated = this.handleValidation();
        if (isValidated) {
            let fields = this.state.fields;
            let data = {
                username: fields['fullName'],
                password: fields['password'],
                email: fields['email'],
                phone: fields['number']
            }
            this.service.registerUser(data).then((response) => {
                if (response.status === 200) {
                    customToast.success('Account created successfully');
                    this.setState({ next: true });
                }
            }, (registerError) => {
                let response = registerError.response
                if (response) {
                    if (response.status === 409) customToast.info(response.data.data);
                    if (response.status === 500) customToast.error("Server Error Occuered");
                }
            })
        }
    }

    render() {
        if (this.state.next) {
            return (
                <div>
                    <ToastContainer fromRegistration='true' />
                    <Redirect to='/login' />
                </div>
            )
        }
        return (
            <div>
                <ToastContainer />
                <form className='form-group' onSubmit={this.submitForm.bind(this)}>

                    <div className="form-group row">
                        <div className="col-sm-2"></div>
                        <label htmlFor="usertype" className="col-sm-2">User</label>
                        <div className="col-sm-6">
                            <select name="userType" className="custom-select mb-3" required value={this.state.fields["userType"]} onChange={this.handleChange.bind(this, "userType")}>
                                <option defaultValue=''>Student/teacher/management</option>
                                <option value="student">Student</option>
                                <option value="teacher">Teacher</option>
                                <option value="management">Management</option>
                            </select>
                            <span style={{ color: "red", fontSize: '12px' }}>{this.state.errors["userType"]}</span>
                        </div>
                        <div className="col-sm-2"></div>
                    </div>


                    <div className="form-group row">
                        <div className="col-sm-2"></div>
                        <label htmlFor="fullName" className="col-sm-2">Username:</label>
                        <div className="col-sm-6">
                            <input type="text" className="form-control" id="fullName" placeholder="Enter Full Name" name="fullName" value={this.state.fields["fullName"]} onChange={this.handleChange.bind(this, "fullName")} />
                            <span style={{ color: "red", fontSize: '12px' }}>{this.state.errors["fullName"]}</span>
                        </div>
                        <div className="col-sm-2"></div>
                    </div>


                    <div className="form-group row ">
                        <div className="col-sm-2"></div>
                        <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">Email</label>
                        <div className="col-sm-6">
                            <input type="email" className="form-control" id="inputEmail3" placeholder="Email" name='email' value={this.state.fields["email"]} onChange={this.handleChange.bind(this, "email")} />
                            <span style={{ color: "red", fontSize: '12px' }}>{this.state.errors["email"]}</span>
                        </div>
                        <div className="col-sm-2"></div>
                    </div>


                    <div className="form-group row">
                        <div className="col-sm-2"></div>
                        <label htmlFor="inputPassword3" className="col-sm-2 col-form-label">Password</label>
                        <div className="col-sm-6">
                            <input type="password" className="form-control" id="inputPassword3" placeholder="Password" name='password' value={this.state.fields["password"]} onChange={this.handleChange.bind(this, "password")} />
                            <span style={{ color: "red", fontSize: '12px' }}>{this.state.errors["password"]}</span>
                        </div>
                    </div>
                    <div className="col-sm-2"></div>


                    <div className="form-group row ">
                        <div className="col-sm-2"></div>
                        <label htmlFor="inputnumber" className="col-sm-2 col-form-label">Number</label>
                        <div className="col-sm-6">
                            <input pattern="\d*" type="number" min="10" name="number" id="number" className="form-control" placeholder="Your 10 digit mobile number" value={this.state.fields["number"]} onChange={this.handleChange.bind(this, "number")} />
                            <span style={{ color: "red", fontSize: '12px' }}>{this.state.errors["number"]}</span>
                        </div>

                        <div className="col-sm-2"></div>
                    </div>


                    <div className="form-group row">
                        <div className="col-sm-12">
                            <div className="form-check">
                                <label className="form-check-label" htmlFor="termsandconditions">
                                    <input name='termsandconditions' className="form-check-input" type="checkbox" id="termsandconditions" value={this.state.fields["conditions"]} onChange={this.enableButton.bind(this)} />
                            I agree to all terms and conditions
                    </label>
                            </div>
                        </div>
                    </div>


                    <div className="form-group row">
                        <div className="col-sm-12">
                            <input disabled={!this.state.conditions} id='registerSubmit' type="submit" className="btn btn-primary button" value='Sign Up' />
                        </div>
                    </div>
                </form >
            </div>
        )
    }
}

export default Form