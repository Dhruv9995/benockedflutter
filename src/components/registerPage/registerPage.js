import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Container, Row } from 'reactstrap';
import './registerPage.css'
import LeftChamber from './../leftChamber/leftChamber';
import TopRightLine from './../fieldComponents/topRightLine';
import Form from './registerForrm/registerForm'


export default class Register extends Component {
    render() {
        return (
            <div className='p-0'>
                <Container fluid={true} className='login'>
                    <Row >
                        <div className="col-md-6 ">
                            <LeftChamber color='#E75353' imageURL='signup.png' fontColor='#FECD4C' />
                        </div>

                        <div className="col-md-6" >
                            <TopRightLine link='/login' text='Already have an account?    ' linkText='    Signin' />

                            <div className="container signinrightText">
                                <div className="row">
                                    <div className="col-md text-center"><h5 style={{ fontWeight: 'bold' }}>Signin to be Nocked!</h5></div>
                                </div>
                                <br />
                                <Form />
                            </div>
                        </div>
                    </Row>
                </Container>
            </div >
        );
    }
}