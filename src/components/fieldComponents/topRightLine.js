import React, { Component } from 'react';

class TopRightLine extends Component {
    state = {}
    render() {
        return (
            <h5 style={{ textAlign: 'right' }}>{this.props.text}
                <a href={this.props.link} style={{ color: '#ea4c89' }}>{this.props.linkText}</a>
            </h5>
        );
    }
}

export default TopRightLine;