import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route, Switch
} from "react-router-dom";
import Landing from './landingPage/landingPage';
import Login from './loginPage/loginPage';
import Register from './registerPage/registerPage';
import PageNotFound from './pageNotFound/pageNotFound';
import Social from './../components/Social/social';
import Welcome from './welcome/welcome';
import Profile from './userProfile/userProfile';
import './App.css';

class App extends Component {
    state = {}
    render() {
        return (
            <div className="App">
                <Router>
                    <Switch>
                        <Route exact path="/" component={Landing} />
                        <Route path='/login' component={Login} />
                        <Route path='/register' component={Register} />
                        <Route path='/welcome' component={Welcome} />
                        <Route path='/social' component={Social} />
                        <Route path='/profile' component={Profile} />
                        <Route path='/user' component={Profile} />
                        <Route component={PageNotFound} />
                    </Switch>
                </Router>
            </div>
        );
    }
}

export default App;