import React from 'react';
import NavBar from './navbar/navbar';
import PageContent from './pageContent/pageContent';

export default class Login extends React.Component {
    render() {
        return <div>
            <NavBar />
            <PageContent />
        </div >
    }
}