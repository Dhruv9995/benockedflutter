import React from 'react';
import {
    Link
} from "react-router-dom";
import './navbar.css'

export default class NavBar extends React.Component {
    render() {
        let alreadyLoggedIn = JSON.parse(localStorage.getItem('userData'));
        return <div >
            <div className="container-fluid">
                <nav className="navbar navbar-expand-md navbar-light bg-faded">
                    <div className="mx-auto order-0">
                        <Link className="navbar-brand mx-auto" to="/">
                            <p className='logoText' style={{ 'padding': '12px 30px 0px 5px', 'fontSize': '40px', 'fontFamily': 'Sacramento, cursive' }}> benocked</p>
                        </Link>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                    </div>
                    <div className="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                                <Link className="nav-link" to="/">Explore
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z" />
                                    </svg>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/">For Student
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z" />
                                    </svg></Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/">|</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/">For Teacher
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z" />
                                    </svg></Link>
                            </li>
                        </ul>
                    </div>

                    {
                        alreadyLoggedIn && alreadyLoggedIn.benocked_id ?
                            <div div className="navbar-collapse collapse w-100 order-3 dual-collapse2">
                                <ul className="navbar-nav ml-auto">
                                    <li className="nav-item">
                                        <Link className="nav-link button" style={{ color: 'white', fontWeight: 'bold' }} to="/social">Social</Link>
                                    </li>
                                </ul>
                            </div> :
                            < div className="navbar-collapse collapse w-100 order-3 dual-collapse2">
                                <ul className="navbar-nav ml-auto">
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/">For Institutions
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                <path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z" />
                                            </svg>
                                        </Link>
                                    </li>

                                    <li className="nav-item">
                                        <Link className="nav-link" to="/login">Signin</Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link button" style={{ color: 'white', fontWeight: 'bold' }} to="/register">Signup</Link>
                                    </li>
                                </ul>
                            </div>
                    }
                </nav>
            </div>
        </div >
    }
}