import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Container, Row } from 'reactstrap';
import './pageContent.css'

export default class PageContent extends React.Component {
    render() {
        return <div className='pageContent'>
            <Container fluid={true} >
                <Row>
                    <div className="col-md-8">
                        <div className=" edtech text-left" >
                            <h1 className="edtext">Be the Next Gen Ed provider
                              <br />
                                Give your institution the technical edge</h1>
                            <div className="edtechinner">
                                <h4 className="spacingED" >Build your virtual classes to Reach more audiance
                                <br />Power your physical classroom with the all new edtech
                                <br />Take education up a notch<br />Reduce your Costs and efforts<br />
                                </h4>
                                <button style={{ "float": "right" }} className="getstarted"><a href='/register' style={{ color: 'white', textDecoration: 'none' }}>Get started</a></button>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4" > <img className="img-fluid" style={{ marginTop: '50px' }} src={require('./../images/done-5.png')} alt="SampleImage" /></div>
                </Row>
            </Container>
        </div>
    }
}