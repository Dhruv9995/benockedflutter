import React, { Component } from 'react';

class DownDropDown extends Component {
    state = {
        isOpen: false
    }
    toggleOpen = () => this.setState({ isOpen: !this.state.isOpen });
    render() {
        const menuClass = `dropdown-menu${this.state.isOpen ? " show" : ""}`;
        return (
            <div className="dropdown" onClick={this.toggleOpen} style={{ maxWidth: '50px' }}>
                <button className="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img style={{ float: 'right' }} src={require('./../assets/svg/arrowdown.svg')} alt='arrowdown' />
                </button>
                <div className={menuClass} aria-labelledby="gedf-drop1">
                    <div className="h6 dropdown-header">{this.props.title}</div>
                    {
                        this.props.buttons && this.props.buttons.map((button) => {
                            return <a className="dropdown-item" href={button.link} key={button.name}>{button.name}</a>
                        })
                    }
                </div>
            </div>
        );
    }
}

export default DownDropDown;