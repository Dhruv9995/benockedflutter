import React, { Component } from 'react';
import HTTPRequests from './../../services/httpRequests';
import customToast from './../../toaster/toaster';
import { ToastContainer } from 'react-toastify';
import { Redirect } from 'react-router-dom';


class LoginForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            fields: {
                email: '',
                password: ''
            },
            errors: {},
            firstTime: false,
            social: false,
            fromRegistration: this.props.fromRegistration
        }
        this.service = new HTTPRequests()
    }

    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        //Email
        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "Email can not be empty";
        }

        if (typeof fields["email"] !== "undefined") {
            let lastAtPos = fields["email"].lastIndexOf('@');
            let lastDotPos = fields["email"].lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["email"] = "Email is not valid";
            }
        }

        // password
        if (!fields["password"]) {
            formIsValid = false;
            errors['password'] = "Password is required"
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    stepValidation(field) {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        // email
        if (field === 'email') {
            if (!fields["email"]) {
                formIsValid = false;
                errors["email"] = "Email can not be empty";
            }

            if (typeof fields["email"] !== "undefined") {
                let lastAtPos = fields["email"].lastIndexOf('@');
                let lastDotPos = fields["email"].lastIndexOf('.');

                if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                    formIsValid = false;
                    errors["email"] = "Email is not valid";
                }
            }
        }

        if (field === 'password') {
            if (!fields["password"]) {
                formIsValid = false;
                errors['password'] = "Password is required"
            }
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
        this.stepValidation(field);
    }

    submitForm(form) {
        form.preventDefault();
        let isValidated = this.handleValidation();
        if (isValidated) {
            let fields = this.state.fields;
            let data = {
                password: fields['password'],
                email: fields['email'],
            }
            this.service.loginUser(data).then((response) => {
                if (response.status === 200) {
                    localStorage.setItem('token', response.data.token);
                    customToast.success('Logged in successfully');
                    this.service.userData({ headers: { Authorization: 'JWT ' + response.data.token } })
                        .then((response) => {
                            localStorage.setItem('userData', JSON.stringify(response.data));
                            this.setState({ firstTime: true });
                        })
                }
            }, (loginError) => {
                if (loginError.response) {
                    let response = loginError.response
                    if (response.status === 400) customToast.error('Wrong Password');
                    if (response.status === 500) customToast.error("Server Error Occuered");
                }
            })
        }
    }

    render() {
        if (this.state.social) {
            return <Redirect to='/social' />;
        }
        if (this.state.firstTime) {
            return <Redirect to='/welcome' />
        }
        if (this.state.fromRegistration) {
            customToast.success('Account created successfully');
        }
        return (
            <div>
                <ToastContainer />
                <form className='form-group' onSubmit={this.submitForm.bind(this)}>
                    <div className="form-group row ">
                        <div className="col-sm-2"></div>
                        <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">Email</label>
                        <div className="col-sm-6">
                            <input type="email" className="form-control" id="inputEmail3" placeholder="Email" name='email' value={this.state.fields["email"]} onChange={this.handleChange.bind(this, "email")} />
                            <span style={{ color: "red", fontSize: '12px' }}>{this.state.errors["email"]}</span>                    </div>
                        <div className="col-sm-2"></div>
                    </div>
                    <div className="form-group row">
                        <div className="col-sm-2"></div>
                        <label htmlFor="inputPassword3" className="col-sm-2 col-form-label">Password</label>
                        <div className="col-sm-6">
                            <input type="password" className="form-control" id="inputPassword3" placeholder="Password" name='password' value={this.state.fields["password"]} onChange={this.handleChange.bind(this, "password")} />
                            <span style={{ color: "red", fontSize: '12px' }}>{this.state.errors["password"]}</span>                    </div>
                    </div>
                    <div className="col-sm-2"></div>
                    <br />
                    <div className="form-group row">
                        <div className="col-sm-12">
                            <input type="submit" className="btn btn-primary button" value='Sign In' />
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default LoginForm;