import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Container, Row } from 'reactstrap';
import LeftChamber from './../leftChamber/leftChamber';
import LoginForm from './LoginForm/LoginForm';
import './loginPage.css'


const Login = () => {
    return (
        <div>
            <Container fluid={true} className='login'>
                <Row>
                    <div className="col-md-6 ">
                        <LeftChamber color='#F5EBDF' imageURL='signin.png' fontColor='black' />
                    </div>
                    <div className="col-md-6" >
                        <h5 style={{ textAlign: 'right' }}>don't have an account ?
                        <a href="/register" style={{ color: '#ea4c89' }}>Signup</a>
                        </h5>
                        <div className="container signinrightText">
                            <div className="row">
                                <div className="col-md text-center">
                                    <h5 style={{ fontWeight: 'bold' }}>
                                        Signin to be Nocked!
                                </h5>
                                </div>
                            </div>
                            <LoginForm />
                        </div>
                    </div>
                </Row>
            </Container>
        </div >
    );
}

export default Login;