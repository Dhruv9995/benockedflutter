import React, { Component } from 'react';
import './leftChamber';

class LeftChamber extends Component {
    state = {
        color: this.props.color,
        imageURL: this.props.imageURL,
        fontColor: this.props.logoColor
    }
    render() {
        return (
            <div className='leftChamber'>
                <div style={{ backgroundColor: this.state.color }}>
                    <div className="row">
                        <div className="col-md ">
                            <div className="container signinlefttext">
                                <a href="/" className='logoText' style={{ fontSize: '80px', color: 'black', textDecoration: 'none' }}>
                                    benocked
                                   </a>
                                <div className="signinfontleft">
                                    Experience the power of
                                              ED Tech<br />
                                                Think Ahead!
                                                Aim Next!
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row no-gutters">
                        <div className="col-md">
                            <img className="img-fluid" src={require('./../assets/images/' + this.props.imageURL)} alt={this.props.imageURL} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default LeftChamber;