import React, { Component } from "react";
import MainNavBar from "./../mainNavBar/mainNavBar";
import { Modal, Tabs, Tab, Button, Spinner } from "react-bootstrap";
import "./profile.css";
import PeopleCards from "./../Social/sideProfile/trending/trendingCards/peopleCard";
import HTTPRequests from "./../services/httpRequests";
import logout from "./../../functions";
import { Redirect } from "react-router-dom";

class PofileUpper extends Component {
  constructor(props) {
    super(props);
    this.userData = !this.props.other
      ? JSON.parse(localStorage.getItem("userData"))
      : JSON.parse(sessionStorage.getItem("otherUserData"))
    this.state = {
      other: this.props.other || false,
      openSettings: false,
      followers: false,
      following: false,
      openEdit: false,
      fields: {
        'currentSkills': this.userData.skills,
        'currentInterests': this.userData.interests
      },
      follow: true,
      loading: false,
      typeAbout: false,
      checked: false,
      firstHit: true,
      loggedOut: false
    };
    this.year = 2000;
    this.years = Array.from(new Array(71), (val, index) => index + this.year);
    this.service = new HTTPRequests();
    this.handle = this.handle.bind(this);
  }

  handle() {
    this.componentDidMount();
  }

  componentDidMount() { }
  openSettings() {
    this.setState({ openSettings: !this.state.openSettings });
  }
  openFollowers() {
    this.setState({ followers: !this.state.followers });
  }
  openFollowing() {
    this.setState({ following: !this.state.following });
  }
  openEdit() {
    this.setState({ openEdit: !this.state.openEdit, openSettings: false });
  }
  async logOut() {
    await logout();
    this.setState({ loggedOut: true });
  }
  addSkill() {
    let currentSkills = this.state.fields["currentSkills"];
    if (currentSkills && currentSkills.split(",").length > 0) {
      let newSkills = currentSkills + "," + this.state.fields["skills"];
      let fields = { ...this.state.fields, currentSkills: newSkills };
      this.setState({ fields: fields }, () => {
        document.getElementById("skills").value = "";
      });
    } else {
      let newSkills = this.state.fields["skills"];
      let fields = { ...this.state.fields, currentSkills: newSkills };
      this.setState({ fields: fields }, () => {
        document.getElementById("skills").value = "";
      });
    }
  }
  removeElement(element) {
    if (element.skill) {
      let currentSkills = this.state.fields.currentSkills.toString().split(",");
      let toBeRemovedSkill = [element.skill];
      let remainingSkills = currentSkills
        .filter((skill) => toBeRemovedSkill.indexOf(skill) === -1)
        .join(",");
      let fields = { ...this.state.fields, currentSkills: remainingSkills };
      this.setState({ fields: fields });
    }
    if (element.interest) {
      let currentInterests = this.state.fields.currentInterests
        .toString()
        .split(",");
      let toBeRemovedInterest = [element.interest];
      let remainingInterests = currentInterests
        .filter((skill) => toBeRemovedInterest.indexOf(skill) === -1)
        .join(",");
      let fields = {
        ...this.state.fields,
        currentInterests: remainingInterests,
      };
      this.setState({ fields: fields });
    }
  }
  addInterests() {
    let currentInterests = this.state.fields["currentInterests"] || '';
    if (currentInterests && currentInterests.split(",").length > 0) {
      let interests = currentInterests + "," + this.state.fields["interests"];
      let fields = { ...this.state.fields, currentInterests: interests };
      this.setState({ fields: fields }, () => {
        document.getElementById("interests").value = "";
      });
    } else {
      let interests = this.state.fields["interests"];
      let fields = { ...this.state.fields, currentInterests: interests };
      this.setState({ fields: fields }, () => {
        document.getElementById("interests").value = "";
      });
    }
  }
  sendData() {
    this.setState({ loading: true });
    let fields = this.state.fields;
    let fieldsName = {
      name: "fullname",
      currentSkills: "skills",
      currentInterests: "interests",
      currentCollege: "college",
      course: "current_course",
      from: "session_from",
      to: "session_to",
    };
    let data = {};
    for (let field in fields) {
      if (field !== "skills" && field !== "interests") {
        let fieldData = fields[field];
        if (fieldsName[field]) field = fieldsName[field];
        if (fieldData.trim() != "")
          data[field] = fieldData;
      }
    }

    this.service.completeProfile(data).then((response) => {
      if (response.status === 200) {
        this.service.userData({}).then((response) => {
          localStorage.removeItem("userData");
          localStorage.setItem("userData", JSON.stringify(response.data));
          this.setState({ loading: false });
          this.props.handler();
        });
      }
    });
  }
  handleChange(field, e) {
    let fields = this.state.fields;
    fields[field] = e.target.value;
  }
  otherUserData(id) {
    if (this.state.firstHit)
      this.service.otherUserData(id).then((response) => {
        if (response.status === 200) {
          sessionStorage.setItem(
            "otherUserData",
            JSON.stringify(response.data)
          );
          this.setState({
            loading: false,
            firstHit: false,
            needToRefresh: false,
          });
        }
      });
  }
  followUser() {
    if (this.state.follow)
      this.service.followUsers(this.props.userId).then((response) => {
        if (response.status === 200 && this.state.firstHit) {
          this.service.otherUserData(this.props.userId).then((response) => {
            if (response.status === 200) {
              sessionStorage.setItem(
                "otherUserData",
                JSON.stringify(response.data)
              );
              this.setState({ follow: false, loading: false });
            }
          });
        }
      });
    else
      this.service.unfollowUsers(this.props.userId).then((response) => {
        if (response.status === 200) {
          this.service.otherUserData(this.props.userId).then((response) => {
            if (response.status === 200) {
              sessionStorage.setItem(
                "otherUserData",
                JSON.stringify(response.data)
              );
              this.setState({ follow: true, loading: false });
            }
          });
        }
      });
  }
  openAbout() {
    this.setState({ typeAbout: true });
  }
  saveAbout() {
    this.setState({ typeAbout: false });
    let allData = JSON.parse(localStorage.getItem("userData"));
    allData["about"] = this.state.fields.about;
    localStorage.setItem("userData", JSON.stringify(allData));
    this.service.completeProfile({ about: this.state.fields.about });
  }
  render() {
    if (this.state.loggedOut) {
      return <Redirect to="/" />;
    }
    let realdata = JSON.parse(localStorage.getItem("userData"));
    let userData = this.userData;
    if (userData.followers && !this.state.checked) {
      userData.followers.map((user) => {
        if (Number(user.Link.split("user/")[1]) === Number(realdata["id"])) {
          this.setState({ follow: false, checked: true });
        }
      });
    }
    // if (this.state.loading) return <Spinner />;
    return (
      <div>
        <div>
          <div className="container-fluid">
            <MainNavBar />
            <hr className="profile_divider m-0 p-0" />
          </div>
          <div className="row no gutters">
            <div className="col-md-2"></div>
            <div className="col-md-8 inset-2">
              <div className="container-fluid profile-container">
                <div className="row ">
                  <div className="col-md-4 ">
                    <div className="mt-8 " style={{ display: "block" }}>
                      <img
                        src={require("./../assets/images/boyprofile.jpg")}
                        className="img-fluid  float-left my-Profile"
                        alt="#"
                      />
                    </div>
                  </div>
                  {this.state.openSettings && (
                    <Modal
                      show={this.state.openSettings}
                      onHide={this.openSettings.bind(this)}
                      backdrop="static"
                      keyboard={false}
                    >
                      <Modal.Header closeButton>
                        <Modal.Title>Settings</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <ul className="list-group list-group-flush">
                          <li className="list-group-item setting">
                            <div className="h6 text-muted setting">
                              <button
                                className="no-dec btn button_edit text-center"
                                onClick={this.openEdit.bind(this)}
                                style={{ width: '100%' }}
                              >
                                Edit Profile
                            </button>
                            </div>
                          </li>
                          <li className="list-group-item">
                            <div className="h6 text-muted">
                              <button
                                className="no-dec btn button_edit text-center"
                                style={{ width: '100%' }}
                              >
                                Change Password
                            </button>
                            </div>
                          </li>
                          {/* <li className="list-group-item">
                          <div className="h6 text-muted">
                            <a className="no-dec" href="#">
                              Privacy
                            </a>
                          </div>
                        </li> */}
                          {/* <li className="list-group-item">
                          <div className="h6 text-muted">
                            <a className="no-dec" href="#">
                              Notifications
                            </a>
                          </div>
                        </li>
                        <li className="list-group-item">
                          <div className="h6 text-muted">
                            <a className="no-dec" href="#">
                              Security
                            </a>
                          </div>
                        </li>
                        <li className="list-group-item">
                          <div className="h6 text-muted">
                            <a className="no-dec" href="#">
                              Account
                            </a>
                          </div>
                        </li> */}
                          <li className="list-group-item">
                            <div className="h6 text-muted">
                              <button
                                className="no-dec btn button_edit text-center"
                                style={{ width: '100%' }}
                              >
                                Help
                            </button>
                            </div>
                          </li>
                          <li className="list-group-item">
                            <div className="h6 text-muted">
                              <button
                                className="no-dec btn button_edit text-center"
                                style={{ width: '100%', backgroundColor: 'none' }}
                              >
                                About Us
                            </button>
                            </div>
                          </li>
                          <li className="list-group-item">
                            <div className="h6 text-muted">
                              <button
                                className="no-dec btn button_edit text-center"
                                onClick={this.logOut.bind(this)}
                                style={{ width: '100%' }}
                              >
                                Logout
                            </button>
                            </div>
                          </li>
                        </ul>
                      </Modal.Body>
                    </Modal>
                  )}
                  {this.state.followers && (
                    <Modal
                      show={this.state.followers}
                      onHide={this.openFollowers.bind(this)}
                      backdrop="static"
                      keyboard={false}
                    >
                      <Modal.Header closeButton>
                        <Modal.Title>Followers</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        {userData.followers &&
                          userData.followers.length > 0 &&
                          userData.followers.map((user) => {
                            return (
                              <PeopleCards
                                handle={this.handle}
                                followUser={user.Name}
                                userId={user.Link.split("user/")[1]}
                                follow={userData.following.filter(e => e.id === user.id).length > 0 ? false : true}
                                profilePic={
                                  user.Profilepic ||
                                  "https://images.unsplash.com/photo-1555445091-5a8b655e8a4a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
                                }
                                location="Chandigarh, India"
                              />
                            );
                          })}
                      </Modal.Body>
                    </Modal>
                  )}
                  {this.state.following && (
                    <Modal
                      show={this.state.following}
                      onHide={this.openFollowing.bind(this)}
                      backdrop="static"
                      keyboard={false}
                    >
                      <Modal.Header closeButton>
                        <Modal.Title>Following</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        {userData.following &&
                          userData.following.length > 0 &&
                          userData.following.map((user) => {
                            return (
                              <PeopleCards
                                followUser={user.Name}
                                userId={user.Link.split("user/")[1]}
                                follow={false}
                                profilePic={
                                  user.Profilepic ||
                                  "https://images.unsplash.com/photo-1555445091-5a8b655e8a4a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
                                }
                                location="Chandigarh, India"
                              />
                            );
                          })}
                      </Modal.Body>
                    </Modal>
                  )}
                  <div className="col-md-8">
                    <div className="row no-gutters">
                      <div className="col-md-6 text-left">
                        <div className="inline-block">
                          <span className="lead name-profile">
                            {userData.fullname ||
                              userData.username ||
                              userData.user}
                          </span>
                          {!this.state.other ? (
                            <img
                              type="button"
                              className="img-fluid gear ml-3"
                              role="button"
                              src={require("./../assets/svg/gear.svg")}
                              onClick={this.openSettings.bind(this)}
                              alt="abc"
                            />
                          ) : (
                              <div
                                className="btn mt-auto mb-auto ml-auto mr-auto btn-outline-primary"
                                type="button"
                                id="follow"
                                value={this.props.userId}
                                onClick={this.followUser.bind(this)}
                              >
                                {this.state.follow ? "Follow" : "Unfollow"}
                              </div>
                            )}
                          <br />
                          <small className="text-muted">
                            @{userData.username || userData.user}
                          </small>
                        </div>
                        <p></p>
                      </div>
                    </div>
                    <br />
                    <div className="row">
                      <div className="col-md-6">
                        {" "}
                        <div className="h6 text-muted">
                          <a
                            type="button"
                            onClick={this.openFollowers.bind(this)}
                            className="no-dec"
                          >
                            Followers<h5> {userData.followers_count || "0"}</h5>
                          </a>
                        </div>
                      </div>
                      <div className="col-md-6">
                        {" "}
                        <div className="h6 text-muted">
                          <a
                            type="button"
                            onClick={this.openFollowing.bind(this)}
                            className="no-dec"
                          >
                            Following<h5>{userData.following_count || "0"}</h5>
                          </a>
                        </div>
                      </div>
                    </div>
                    <div className="row mt-3">
                      <div className="col-md-12">
                        <p className="lead font-weight-bold text-muted">
                          About
                        {!this.state.other ? (
                            this.state.typeAbout ? (
                              <img
                                type="button"
                                className="img-fluid save ml-3"
                                role=" button"
                                src={require("./../assets/svg/saveItem.svg")}
                                onClick={this.saveAbout.bind(this)}
                              />
                            ) : (
                                <img
                                  type="button"
                                  className="img-fluid edit ml-3"
                                  role=" button"
                                  src={require("./../assets/svg/editButton.svg")}
                                  onClick={this.openAbout.bind(this)}
                                />
                              )
                          ) : (
                              <span></span>
                            )}
                        </p>
                        {!this.state.typeAbout ? (
                          <p className="lead"> {userData.about}</p>
                        ) : (
                            <textarea
                              type="form-control"
                              style={{ resize: "none" }}
                              name="about"
                              rows="3"
                              onChange={this.handleChange.bind(this, "about")}
                            />
                          )}
                      </div>
                    </div>
                  </div>
                  {this.state.openEdit && (
                    <Modal
                      show={this.state.openEdit}
                      onHide={this.openEdit.bind(this)}
                      backdrop="static"
                      keyboard={false}
                      size="sl"
                      aria-labelledby="contained-modal-title-vcenter"
                      centered
                    >
                      <Modal.Header closeButton>
                        <Modal.Title>Profile Intakes</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <Tabs defaultActiveKey="Personal" id="tabs">
                          <Tab eventKey="Personal" title="Personal">
                            <table className="table">
                              <tbody>
                                <tr>
                                  <td>Name</td>
                                  <td>
                                    <input
                                      type="text"
                                      name="name"
                                      className="form-control mb-3"
                                      placeholder={userData.fullname || "Your full name"}
                                      required
                                      onChange={this.handleChange.bind(
                                        this,
                                        "name"
                                      )}
                                    ></input>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Date Of Birth</td>
                                  <td>
                                    <input
                                      type="date"
                                      name="dob"
                                      className="custom-select mb-3"
                                      defaultValue={userData.dob}
                                      required
                                      onChange={this.handleChange.bind(
                                        this,
                                        "dob"
                                      )}
                                    ></input>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Gender</td>
                                  <td>
                                    <label>
                                      <input
                                        className="ml-3 mr-1 mb-3"
                                        type="radio"
                                        name="gender"
                                        value="male"
                                        checked={userData.gender === "male" ? true : false}
                                        onChange={this.handleChange.bind(
                                          this,
                                          "gender"
                                        )}
                                      />{" "}
                                    Male
                                  </label>
                                    <label>
                                      <input
                                        className="ml-3 mr-1 mb-3"
                                        type="radio"
                                        name="gender"
                                        value="female"
                                        checked={userData.gender === "female" ? true : false}
                                        onChange={this.handleChange.bind(
                                          this,
                                          "gender"
                                        )}
                                      />{" "}
                                    Female
                                  </label>
                                    <label>
                                      <input
                                        className="ml-3 mr-1 mb-3"
                                        type="radio"
                                        name="gender"
                                        value="other"
                                        checked={userData.gender === "other" ? true : false}
                                        onChange={this.handleChange.bind(
                                          this,
                                          "gender"
                                        )}
                                      />{" "}
                                    Other
                                  </label>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Skills</td>
                                  <td>
                                    <input
                                      id="skills"
                                      type="text"
                                      className="form-control mb-3"
                                      placeholder="Press enter after entering each skill"
                                      onChange={this.handleChange.bind(
                                        this,
                                        "skills"
                                      )}
                                      onKeyDown={(event) => {
                                        event.persist();
                                        if (event.key === "Enter")
                                          this.addSkill();
                                      }}
                                    ></input>
                                  </td>
                                </tr>
                                {this.state.fields["currentSkills"] &&
                                  this.state.fields["currentSkills"].split(",")
                                    .length > 0 && (
                                    <tr>
                                      <td>Click to remove</td>
                                      <td>
                                        {this.state.fields["currentSkills"]
                                          .split(",")
                                          .map((skill) => {
                                            return (
                                              <div
                                                className="button"
                                                style={{ wordWrap: "break-word" }}
                                                onClick={this.removeElement.bind(
                                                  this,
                                                  { skill }
                                                )}
                                                value={skill}
                                                key={skill}
                                              >
                                                {skill}
                                              </div>
                                            );
                                          })}
                                      </td>
                                    </tr>
                                  )}
                                <tr>
                                  <td>Interests</td>
                                  <td>
                                    <input
                                      id="interests"
                                      type="text"
                                      className="form-control mb-3"
                                      placeholder="Press enter after entering each interest"
                                      onChange={this.handleChange.bind(
                                        this,
                                        "interests"
                                      )}
                                      onKeyDown={(event) => {
                                        event.persist();
                                        if (event.key === "Enter") {
                                          this.addInterests();
                                        }
                                      }}
                                    ></input>
                                  </td>
                                </tr>
                                {this.state.fields["currentInterests"] &&
                                  this.state.fields["currentInterests"].split(",").length > 0 && (
                                    <tr>
                                      <td>Click to remove</td>
                                      <td>
                                        {this.state.fields["currentInterests"]
                                          .split(",")
                                          .map((interest) => {
                                            return (
                                              <div
                                                className="button"
                                                style={{ wordWrap: "break-word" }}
                                                onClick={this.removeElement.bind(
                                                  this,
                                                  { interest }
                                                )}
                                                value={interest}
                                                key={interest}
                                              >
                                                {interest}
                                              </div>
                                            );
                                          })}
                                      </td>
                                    </tr>
                                  )}
                              </tbody>
                            </table>
                          </Tab>
                          <Tab eventKey="Qualification" title="Qualification">
                            <table className="table">
                              <tbody>
                                <tr>
                                  <td>Current Institution</td>
                                  <td>
                                    <select
                                      name="currentCollege"
                                      className="form-control mb-3"
                                      required
                                      value={this.state.fields["currentCollege"]}
                                      onChange={this.handleChange.bind(
                                        this,
                                        "currentCollege"
                                      )}
                                    >
                                      <option defaultValue="">
                                        Select your college
                                    </option>
                                      {userData.colleges &&
                                        userData.colleges.map((college) => {
                                          return (
                                            <option value={college}>
                                              {college}
                                            </option>
                                          );
                                        })}
                                    </select>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Stream</td>
                                  <td>
                                    <input
                                      type="text"
                                      name="stream"
                                      className="form-control mb-3"
                                      required
                                      defaultValue={userData.stream || null}
                                      placeholder={(userData.stream && userData.stream != "") ? userData.stream : "Your subject stream"}
                                      onChange={this.handleChange.bind(
                                        this,
                                        "stream"
                                      )}
                                    ></input>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Current course</td>
                                  <td>
                                    <input
                                      type="text"
                                      name="course"
                                      className="form-control mb-3"
                                      required
                                      placeholder={(userData.current_course && userData.current_course != "") ? userData.current_course : "Your course"}
                                      onChange={this.handleChange.bind(
                                        this,
                                        "course"
                                      )}
                                    ></input>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Semester</td>
                                  <td>
                                    <input
                                      type="text"
                                      name="semester"
                                      className="form-control mb-3"
                                      required
                                      placeholder={(userData.semester && userData.semester != "") ? userData.semester : "Your semester"}
                                      onChange={this.handleChange.bind(
                                        this,
                                        "semester"
                                      )}
                                    ></input>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Session</td>
                                  <td>
                                    <span>
                                      <select
                                        name="from"
                                        className="custom-select mb-3"
                                        required
                                        value={this.state.fields["from"]}
                                        defaultValue={userData.session_from ? userData.session_from : '2020'}
                                        onChange={this.handleChange.bind(
                                          this,
                                          "from"
                                        )}
                                      >
                                        <option defaultValue="">From</option>
                                        {this.years &&
                                          this.years.map((year) => {
                                            return (
                                              <option value={year}>{year}</option>
                                            );
                                          })}
                                      </select>
                                    </span>
                                    <span>
                                      <select
                                        name="to"
                                        className="custom-select mb-3"
                                        required
                                        value={this.state.fields["to"]}
                                        defaultValue={userData.session_to ? userData.session_to : '2020'}
                                        onChange={this.handleChange.bind(
                                          this,
                                          "to"
                                        )}
                                      >
                                        <option defaultValue="">To</option>
                                        {this.years &&
                                          this.years.map((year) => {
                                            return (
                                              <option value={year}>{year}</option>
                                            );
                                          })}
                                      </select>
                                    </span>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </Tab>
                          <Tab eventKey="Contact" title="Contact">
                            <table className="table">
                              <tbody>
                                <tr>
                                  <td>Contact Number</td>
                                  <td>
                                    <input
                                      type="text"
                                      name="phone"
                                      className="form-control mb-3"
                                      required
                                      value={userData.phone_no}
                                      onChange={this.handleChange.bind(
                                        this,
                                        "phone"
                                      )}
                                    ></input>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Email Address</td>
                                  <td>
                                    <input
                                      type="text"
                                      name="email"
                                      className="form-control mb-3"
                                      value={userData.email}
                                      onChange={this.handleChange.bind(
                                        this,
                                        "email"
                                      )}
                                    ></input>
                                  </td>
                                </tr>
                                <tr>
                                  <td>City</td>
                                  <td>
                                    <input
                                      type="text"
                                      name="city"
                                      className="form-control mb-3"
                                      required
                                      placeholder={userData.city ? userData.city : "The city you live in"}
                                      onChange={this.handleChange.bind(
                                        this,
                                        "city"
                                      )}
                                    ></input>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </Tab>
                        </Tabs>
                      </Modal.Body>
                      <Modal.Footer>
                        <Button
                          variant="secondary"
                          onClick={this.openEdit.bind(this)}
                        >
                          Close
                      </Button>
                        <Button
                          variant="primary"
                          onClick={this.sendData.bind(this)}
                        >
                          Save changes
                      </Button>
                      </Modal.Footer>
                    </Modal>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PofileUpper;