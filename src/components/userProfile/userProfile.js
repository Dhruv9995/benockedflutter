import React, { Component } from 'react';
import ProfileUpper from './profileUpper';
import './profile.css'
import { Tabs, Tab, Spinner } from 'react-bootstrap';
import HTTPRequests from './../services/httpRequests';
import Loader from 'react-loader-spinner';
import { Redirect } from 'react-router-dom';
import logout from './../../functions';

class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            firstHit: true,
            needToRefresh: true,
            expired: false,
            mine: true,
            userDetails: []
        }
        this.service = new HTTPRequests();
        this.handler = this.handler.bind(this);
    }

    handler() {
        this.setState({ needToRefresh: false, loading: true });
        this.componentDidMount();
    }

    componentDidMount() {
        let mine = window.location.pathname.split('/')[1] === 'user' ? false : true;
        this.setState({ mine });
        window.location.pathname.split('/')[1] === 'user' ? this.otherUserData(window.location.pathname.split('/')[2]) : this.userData();
        let userDetails = window.location.pathname.split('/')[1] === 'user' ? JSON.parse(sessionStorage.getItem('otherUserData')) : JSON.parse(localStorage.getItem('userData'));
        this.setState({ userDetails });
    }
    userData() {
        this.service.userData({})
            .then((response) => {
                if (response.status === 200) {
                    localStorage.removeItem('userData');
                    localStorage.setItem('userData', JSON.stringify(response.data))
                    this.setState({ loading: false, firstHit: false, needToRefresh: false });
                }
            }, async (dataError) => {
                if (dataError.response) {
                    let response = dataError.response
                    if (response.status == 401) {
                        await logout();
                        this.setState({ expired: true });
                    }
                }
            })
    }
    otherUserData(id) {
        this.service.otherUserData(id)
            .then((response) => {
                if (response.status === 200) {
                    sessionStorage.setItem('otherUserData', JSON.stringify(response.data));
                    this.setState({ loading: false, firstHit: false, needToRefresh: false });
                }
            }, async (dataError) => {
                if (dataError.response) {
                    let response = dataError.response
                    if (response.status == 401) {
                        await logout();
                        this.setState({ expired: true });
                    }
                }
            })
    }
    render() {
        let mine = this.state.mine;
        let userDetails = this.state.userDetails;
        if (this.state.expired) {
            return <Redirect to='/login' />;
        }
        return (
            <div>
                {
                    (this.state.loading || userDetails == null) ?
                        <div style={{ top: '40%', bottom: '40%', left: '40%', right: '40%', position: 'absolute' }}>
                            <Loader type="Grid" color="#ea4c89" height={200} width={200} />
                        </div>
                        : (< div >
                            <ProfileUpper other={!mine} userId={window.location.pathname.split('/')[2]} handler={this.handler} />
                            <div className="row">
                                <div className="col-md-4 offset-1 inset-7 text-muted">{mine ? 'My Account Information' : 'Account Information'}</div>
                            </div>
                            <br />
                            <hr className="divider" />
                            {
                                (mine || (!mine && !userDetails.isProfilePrivate)) ?
                                    <div className="container-fluid">
                                        <div className="row ">
                                            <div className="col-md-12 ">
                                                <div className="container">
                                                    <Tabs className="nav nav-pills" defaultActiveKey="Details" id="uncontrolled-tab-example">
                                                        <Tab className="nav-item nav-link btn-outline-light" eventKey="Details" title="Details">
                                                            <div id="P_details" className="container tab-pane ">
                                                                <br />
                                                                <div className="row">
                                                                    <div className="col-md-7 offset-1">
                                                                        <div className="row">
                                                                            <div className="col-md-5">
                                                                                <h4>Name:</h4>
                                                                            </div>
                                                                            <div className="col-md-7">
                                                                                <div className="container detail text-center">{userDetails.fullname || userDetails.username || userDetails.user}</div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="row">
                                                                            <div className="col-md-5">
                                                                                <h4>DOB:</h4>
                                                                            </div>
                                                                            <div className="col-md-7 ">
                                                                                <div className="container detail text-center">{userDetails.dob || '**/**/****'}</div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="row">
                                                                            <div className="col-md-5">
                                                                                <h4>Gender:</h4>
                                                                            </div>
                                                                            <div className="col-md-7">
                                                                                <div className="container detail text-center">{userDetails.gender || '-'}</div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="row">
                                                                            <div className="col-md-5">
                                                                                <h4>Current Institution:</h4>
                                                                            </div>
                                                                            <div className="col-md-7">
                                                                                <div className="container detail text-center">{userDetails.college || '-'}</div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="row">
                                                                            <div className="col-md-5">
                                                                                <h4>Current Course:</h4>
                                                                            </div>
                                                                            <div className="col-md-7">
                                                                                <div className="container detail text-center">{userDetails.current_course || 'B.Com'}</div>
                                                                            </div>
                                                                        </div>

                                                                        <div className="row">
                                                                            <div className="col-md-5">
                                                                                <h4>Semester:</h4>
                                                                            </div>
                                                                            <div className="col-md-7 ">
                                                                                <div className="container detail text-center">{userDetails.semester || '-'}</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-md-4">
                                                                        <div className="row" style={{ minHeight: '30px' }}>
                                                                            <div className="col-md-2">
                                                                                <h4>Skills </h4>
                                                                            </div>
                                                                            <div className="col-md-10" >
                                                                                <div className="container text-center">
                                                                                    <h4>
                                                                                        {
                                                                                            userDetails.skills && userDetails.skills.split(',').map((skill) => {
                                                                                                return <span className="badge bn_styling badge-danger" key={skill}>{skill}</span>
                                                                                            })
                                                                                        }
                                                                                    </h4>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="row mb-3">
                                                                            <div className="col-md-2">
                                                                                <h4>Interest </h4>
                                                                            </div>
                                                                            <div className="col-md-10">
                                                                                <div className="container text-center">
                                                                                    <h4>
                                                                                        {
                                                                                            userDetails.interests && userDetails.interests.split(',').map((interest) => {
                                                                                                return <span className="badge bn_styling badge-danger" key={interest}>{interest}</span>
                                                                                            })
                                                                                        }
                                                                                    </h4>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="row">
                                                                            <div className="col-md-5">
                                                                                <h4>Contact Number:</h4>
                                                                            </div>
                                                                            <div className="col-md-7">
                                                                                <div className="container detail text-center">{userDetails.phone_no || '+91**********'}</div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="row">
                                                                            <div className="col-md-5">
                                                                                <h4>Email Address:</h4>
                                                                            </div>
                                                                            <div className="col-md-7 ">
                                                                                <div className="container detail text-center">{userDetails.email}</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Tab>
                                                    </Tabs>
                                                </div>
                                            </div>
                                        </div>
                                    </div> :
                                    null
                            }
                        </div >)
                }
            </div>
        );
    }
}

export default Profile;