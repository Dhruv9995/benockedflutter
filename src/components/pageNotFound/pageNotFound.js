import React from 'react';
import { withRouter } from 'react-router-dom'
import './pageNotFound.css'

function PageNotFound() {
    return <div>
        <div id="notfound">
            <div class="notfound">
                <div class="notfound-404">
                    <h1>Oops!</h1>
                </div>
                <h2>404 - Page not found</h2>
                <p>The page you are looking for might have been removed had its name changed or is temporarily unavailable.</p>
                <a href="/">Go To Homepage</a>
            </div>
        </div>
    </div>

}

export default withRouter(PageNotFound);