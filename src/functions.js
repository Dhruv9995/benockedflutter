function logout() {
  return new Promise(async (resolve, reject) => {
    localStorage.clear();
    sessionStorage.clear();
    return resolve();
  });
}

export default logout;
